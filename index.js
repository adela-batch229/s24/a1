// Exponent Operator
let getCube = 5;
// Template Literals
console.log(`The cube of ${getCube} is ${getCube**3}`)
// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
let [houseNumber, city, country, postalCode] = address;
console.log(`I live at ${houseNumber} ${city}, ${country} ${postalCode} `)

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
let {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}`)

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];
let loopingNumber = numbers.forEach((num) =>console.log(num))

// Javascript Classes
class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let dog1 = new Dog("Ken Ken", "1","Pug")
let dog2 = new Dog("Choco", "2", "Pug-tzu")

console.log(dog1);
console.log(dog2);
